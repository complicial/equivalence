\contentsline {section}{\tocsection {}{}{Introduction}}{2}{section*.2}%
\contentsline {section}{\tocsection {}{}{Acknowledgements}}{4}{section*.3}%
\contentsline {section}{\tocsection {}{}{Notation}}{5}{section*.4}%
\contentsline {section}{\tocsection {}{1}{Preliminaries}}{5}{section.1}%
\contentsline {subsection}{\tocsubsection {}{1.1}{Marked simplicial sets and marked-simplicial categories}}{5}{subsection.1.1}%
\contentsline {subsection}{\tocsubsection {}{1.2}{Scaled simplicial sets and $\infty $-bicategories}}{9}{subsection.1.2}%
\contentsline {subsection}{\tocsubsection {}{1.3}{Stratified sets}}{12}{subsection.1.3}%
\contentsline {section}{\tocsection {}{2}{Outer cartesian fibrations}}{14}{section.2}%
\contentsline {subsection}{\tocsubsection {}{2.1}{Outer fibrations and cartesian edges}}{15}{subsection.2.1}%
\contentsline {subsection}{\tocsubsection {}{2.2}{The join and slice constructions}}{17}{subsection.2.2}%
\contentsline {subsection}{\tocsubsection {}{2.3}{Mapping categories}}{22}{subsection.2.3}%
\contentsline {subsection}{\tocsubsection {}{2.4}{Cartesian lifts of natural transformations}}{27}{subsection.2.4}%
\contentsline {section}{\tocsection {}{3}{Thin triangles in weak $\infty $-bicategories}}{30}{section.3}%
\contentsline {section}{\tocsection {}{4}{The moving lemma}}{32}{section.4}%
\contentsline {section}{\tocsection {}{5}{Weak $\infty $-bicategories are $\infty $-bicategories}}{38}{section.5}%
\contentsline {section}{\tocsection {}{6}{The Cisinski model structure for $\infty $-bicategories}}{44}{section.6}%
\contentsline {section}{\tocsection {}{7}{The main equivalence}}{45}{section.7}%
\contentsline {section}{\tocsection {}{8}{The homotopy $2$-category and the scaled $2$-nerve}}{53}{section.8}%
\contentsline {section}{\tocsection {Appendix}{A}{Recollections on Cisinski's and Olschok's theory}}{56}{appendix.A}%
\contentsline {section}{\tocsection {Appendix}{}{References}}{58}{section*.25}%
